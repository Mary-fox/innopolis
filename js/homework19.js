"use strict";

//Упражнение 1
let a='$100';
let b='300$';
let summ = Number(a.slice (1)) + Number(b.slice (0, 3));
console.log (summ);
// Ваше решениеconsole.log(summ); изменить на replace
// Должно быть 400

//Упражнение 1 вариант 2
let a1='$100';
let b1='300$';
let summ2 = Number(a1.replace(/[^0-9]/g, '')) + Number(b1.replace(/[^0-9]/g, ''));
console.log (summ2);
//Упражнение 2

let message = '  привет, медвед  ';
message = message.trim();
message = message[0].toUpperCase() + message.slice(1);
console.log(message);

//Упражнение 3
let number = Number(prompt('Сколько вам лет?'));
let result;
if (number >= 0 && number <= 3) {
    result = ('младенец');
} else if (number >= 4 && number <= 11) {
    result = ('ребенок');
} else if (number >= 12 && number <= 18) {
    result = ('подросток');
} else if (number >= 19 && number <= 40) {
    result = ('познаете жизнь');
} else if (number >= 41 && number <= 80) {  
    result = ('познали жизнь');
} else if (number >= 81) {
    result = ('долгожитель');
}
else { 
    number = ("неизвестное количество  лет");
    result = ('ввели некорректные данные');
}
alert(`Вам ${number} и вы ${result}`);    

//Упражнение 4
let message1 = 'Я работаю со строками как профессионал';
let count = message1.split(' ').length;
console.log(count);

//Упражнение 4 вариант 2
let message2 = 'Я работаю со строками как профессионал';
let count1 = message2.split(' ');
let filterCount = count1.filter((letter) => letter !== ' ').length;
console.log(filterCount);

