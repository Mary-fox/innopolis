// document.addEventListener("DOMContentLoaded", () => {
    //скрипт начинаtn выполняться только когда HTML загружен и обработан
    "use strict";
    //Валидация формы
    const form = document.querySelector("form");
    const nameForm = document.querySelector(".form__name");
    const rating = document.querySelector(".form__rating");
    const errorName = document.querySelector(".error__name");
    const errorRating = document.querySelector(".error__rating");
    const formAll = form.querySelectorAll('input, textarea');

    // const btn = document.querySelector (".form__btn");
    let removeErrorName = () => {
        errorName.style.opacity = '0';
        nameForm.classList.remove('error');
    };
    let removeErrorRating = () => {
        errorRating.style.opacity = '0';
        rating.classList.remove('error');
    };

    form.addEventListener("submit", (event) => {
        event.preventDefault(); //отменяем поведение по умолчанию    
        validName();
        validRating();
        clearLocalStorage();

        if (validName(nameForm) === false && validRating(rating) === false ) {
            errorRating.style.opacity = '0';
        };

        nameForm.addEventListener('input', removeErrorName);
        rating.addEventListener('input', removeErrorRating);
        });


    function validName() {
        nameForm.value = nameForm.value.replace(/\s+/g, ' ').trim();
        if (nameForm.value === "" || nameForm.value === null ) {
            errorName.style.opacity = '1';
            nameForm.classList.add('error');
            errorName.textContent = 'Вы забыли указать имя и фамилию';
            return false;
        } else if (nameForm.value.length < 2) {
            nameForm.classList.add('error');
            errorName.style.opacity = '1';
            errorName.textContent = 'Имя не может быть короче 2-х символов';
            return false;
        }
     };

     function validRating() {
        let ratingVal = Number(rating.value);
        if (rating.value === '' || ratingVal > 5 || ratingVal < 1 || !isFinite(ratingVal)) {
            rating.classList.add('error');
            errorRating.style.opacity = '1';
            errorRating.textContent = 'Оценка должна быть от 1 до 5'
            return false;
        };
     };


//homework 25 добавление localStorage
    formAll.forEach(function(e) {
        if(e.value === '') {
            e.value = localStorage.getItem(e.name); //если данные уже записаны в LS-вставляем их в поля формы
        };
         e.addEventListener('input', function() {
            localStorage.setItem(e.name, e.value); // записываем данные в LS 
         });
    })
    function clearLocalStorage(){
        if(validName(nameForm) !== false && validRating(rating) !== false ) {
            localStorage.removeItem("name");
            localStorage.removeItem("rating");
            localStorage.removeItem("review");
        };
    };

    //Промежуточная аттестация добавление/удаление товара из корзины

    let count = document.querySelector('.header__icon-cart-counter');
    let btnCart = document.querySelector('.important-btn__btn');
    let productId = document.querySelector('[name="product-id"]').value;
    let cart = [];
    //
    function changeBtnProductAdded () {
        btnCart.textContent = 'Товар уже в корзине';
        btnCart.classList.add('important-btn__add');  
        count.classList.remove('counter__empty');
    };
    function changeBtnProductNoAdded () {
        btnCart.innerHTML = 'Добавить в корзину';
        btnCart.classList.remove('important-btn__add');
    };

    function deleteCounter() {
        count.classList.add('counter__empty'); 
    };

    // функция добавления в корзину
    function addToCart(e) {  
        if (cart.indexOf(e) === -1) {
            cart.push(e);
            localStorage.setItem("All carts", cart);
            count.textContent = cart.length;
            changeBtnProductAdded();
        }
    };
     // функция удаления из корзины
    function removeFromCart(e) {
        let index = cart.indexOf(e);
        if (index > -1) {
        cart.splice(index, 1);
        localStorage.removeItem("All carts", cart);
        btnCart.innerHTML = 'Добавить в корзину';
        count.textContent = cart.length;
        changeBtnProductNoAdded();
        } 
        if (cart.length === 0) {
            deleteCounter();
        }
    };

    btnCart.addEventListener('click', (e) => {
       if (cart.length > 0) {
        removeFromCart(productId);
       } else {
        addToCart(productId);
        }
    });
      //отрабатывает при перезагрузке страницы
    document.addEventListener("DOMContentLoaded", () => { 
        count.textContent = cart.length;
        if (cart.length === 0) {
            deleteCounter();
        }
        if (localStorage.getItem("All carts")) {
            changeBtnProductAdded();
        }
    });
    if (localStorage.getItem('All carts')) {
        cart = (localStorage.getItem("All carts")).split(','); 
    };
          
// });   

