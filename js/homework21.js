"use strict";

//Упражнение 1
function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }
    return true;
}

 let user= {};
 alert(isEmpty(user)); //true


/** 
 * функция возвращает true, если у объекта нет свойств, иначе false
 * 
 * @param {object} obj - объект;
 * @param {string} key - объявленная переменная внутри цикла(свойство объекта);
 * @returns {boolean} - возвращает булевое значение в зависимости от того если ли свойства объекта или нет;
 */


//Упражнение 2 в файле data.js


//Упражнение 3
let salaries= {
John: 100000,
Ann: 160000,
Pete: 130000,
};
//вариант 1
// let sum = 0;
// function raiseSalary(perzent) {
//     let salaries2 = {
//     };
//     for (let key in salaries) {
//         salaries2[key] = salaries[key] + (Math.floor(salaries[key]* (perzent) / 100)) ;
//         sum += salaries [key];
//     }
//     return salaries2;
// }

// console.log(raiseSalary(5));
// console.log(sum);

//вариант 2

 let sum = 0;
function raiseSalary(perzent) {
    let salaries2 = {
    };
    for (let key in salaries) {
        let raise = Math.floor(salaries[key]* (perzent) / 100) ;
        salaries2[key] = salaries[key] + raise;
        sum += salaries [key];
    }
    return salaries2;
}

console.log(raiseSalary(5));
console.log(sum);

/** 
 * функция повышает зарплату на  определенный процент и расчитывает общую сумму
 * 
 * @param {object} salaries - Объект, содержащий свойства;
 * @param {string} key - объявленная переменная внутри цикла(свойство объекта);
 * @param {number} perzent - процент для увеличения/уменьшения параметров объекта;
 * @returns {numbers} - возвращает сумму объекта;
 */