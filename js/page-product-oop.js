"use strict";

const form = document.querySelector("form");
const nameForm = document.querySelector(".form__name");
const rating = document.querySelector(".form__rating");
const errorName = document.querySelector(".error__name");
const errorRating = document.querySelector(".error__rating");
const formAll = form.querySelectorAll('input, textarea');

class AddReviewForm {
    constructor(form, nameForm, rating) {
        this.form = form;
        this.nameForm = nameForm;
        this.rating = rating;       
    }  

validName() {
    nameForm.value = this.nameForm.value.replace(/\s+/g, ' ').trim();
    if (nameForm.value === "" || nameForm.value === null ) {
        errorName.style.opacity = '1';
        nameForm.classList.add('error');
        errorName.textContent = 'Вы забыли указать имя и фамилию';
        return false;
    } else if (nameForm.value.length < 2) {
        nameForm.classList.add('error');
        errorName.style.opacity = '1';
        errorName.textContent = 'Имя не может быть короче 2-х символов';
        return false;
    }

 };
validRating() {
    let ratingVal = Number(this.rating.value);
    if (this.rating.value === '' || ratingVal > 5 || ratingVal < 1 || !isFinite(ratingVal)) {
        rating.classList.add('error');
        errorRating.style.opacity = '1';
        errorRating.textContent = 'Оценка должна быть от 1 до 5'
        return false;
    };

 };
init()  {
     form.addEventListener("submit", (event) => {
        event.preventDefault(); //отменяем поведение по умолчанию    
        this.validName();
        this.validRating();
        
        this.nameForm.addEventListener('input', function() {
            errorName.style.opacity = '0';
            nameForm.classList.remove('error'); 
        });

        this.rating.addEventListener('input',  function() {
        errorRating.style.opacity = '0';
        rating.classList.remove('error');
        });

        if (this.validName() === false && this.validRating() === false ) {
            errorRating.style.opacity = '0';
        };
        if(this.validName() !== false && this.validRating() !== false ) localStorage.clear();
        });
    }
getText() {
    formAll.forEach(function(e) {
        if(e.value === '') {
            e.value = localStorage.getItem(e.name); //если данные уже записаны в LS-вставляем их в поля формы
        };
        e.addEventListener('input', function() {
            localStorage.setItem(e.name, e.value); // записываем данные в LS 
        });
    })

}    
}
let testForm = new AddReviewForm(form, nameForm, rating);
testForm.init();
testForm.getText();





        

