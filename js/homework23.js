"use strict";

//Упражнение 1

let choiceNumber = parseInt(prompt("Введите число"));
// let timer = setInterval (() => {
//     if (!(choiceNumber)) {
//         clearInterval(timer);
//         console.log("Ошибка! Введено не число!");
//     } else {
//         console.log("Осталось", choiceNumber); 
//         choiceNumber--;
//         }
//     if (choiceNumber === 0) {
//         clearInterval(timer);
//         console.log("Время вышло!")
//     } 
// }, 1000);


//Упражнение 1 через promise 

let promise = new Promise (function (resolve, reject) {
    let timer2 = setInterval (() => {
        if (!(choiceNumber)) {
            clearInterval(timer2);
            reject();
        } else { 
            console.log("Осталось", choiceNumber); 
            choiceNumber--;
            }
        if (choiceNumber === 0) {
            clearInterval(timer2);
            resolve("ОК! Время вышло...");
        } 
    }, 1000);
});

promise
    // Выполнится когда внутри обещания вызвали resolve
    .then((result) => {
        console.log(result); 
    })
    // Выполнится когда будет вызван reject
    .catch(() =>  {
        console.log("Ошибка! Введено не число")
    });


    //Упражение 2
console.time('promise1Way');
 let promise1 = fetch("https://reqres.in/api/users");

 promise1
 .then(function (response) {
   return response.json();
   //ответ перевели в формат json 
 })
 .then((body) => {
    console.log(body);
    console.log(`Получили пользователей: ${body.data.length}`);
    body.data.forEach(element => {
        //console.log("-" + " " + element.last_name + " "  + element.first_name + " " + "(" + element.email + ")")  
        console.log(`- ${element.last_name} ${element.first_name} (${element.email})`)    
    }); 
 })
 .catch ((response) => {
    console.log("Ошибка доступа к данным!")
 });

console.timeEnd('promise1Way'); //таймер для подсчета времени обработки запроса