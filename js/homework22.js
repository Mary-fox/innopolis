"use strict";

//Упражнение 1

let arr1 = [1, 5, '3', -3, 'i'];
let arr2 = [10, 20, '3', 30];
let arr3 = ['no', 500, -0.5, false];

function getSumm(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'number') {
            sum += arr[i];
        } else {
            continue;
        }
    }
    return sum;
}
console.log(getSumm(arr1));
console.log(getSumm(arr2));
console.log(getSumm(arr3));

//Упражнение 2 в data.js

//Упражнение 3

// let cart = [];

// function addToCart (value) {
//     let set = new Set(cart);
//     set.add(value);
//      return cart = [...set];
// }

// function removeFromCart(value) {
//     let set = new Set(cart);
//     set.delete(value);
//     return cart = [...set];
// }

// addToCart(900);
// addToCart(121);
// addToCart(1111);
// addToCart(1111);
// removeFromCart(121);

// console.log(cart);

// вариант 2 без Set

let cart = [4884];

function addToCart(item) {  
    if (cart.indexOf(item) === -1) {
       cart.push(item);
    }
};
  function removeFromCart(item) {
    let index = cart.indexOf(item);
    if (index > -1) {
    cart.splice(index, 1);
    }
  };

  addToCart(3456);
  addToCart(121);
  addToCart(1111);
  addToCart(1111);
  removeFromCart(121);

  console.log(cart);


  //Найти местоположение по значению можно с помощью indexOf() , который возвращает индекс для первого вхождения данного значения или -1, если его нет в массиве.